cmake_minimum_required(VERSION 3.20)
project(EmeraldValley VERSION 1.0.0)

find_package(X11 REQUIRED)
find_package(OpenGL REQUIRED)
find_package(Threads REQUIRED)
find_program(CLANG_TIDY_EXE NAMES "clang-tidy" REQUIRED)


include_directories(
        ${CMAKE_SOURCE_DIR}/include
        ${CMAKE_SOURCE_DIR}/include/raylib
        ${CMAKE_SOURCE_DIR}/include/imgui
)

set(CLANG_TIDY_COMMAND "${CLANG_TIDY_EXE}"
        "-checks=-*,modernize-*"
        #"--export-fixes=out.yaml"
        "--fix"
)

set(CMAKE_CXX_STANDARD 17)
set(CMAKE_POSITION_INDEPENDENT_CODE ON) # -fpic
set(CMAKE_CXX_FLAGS_RELEASE_INIT "-O2")
set(CMAKE_CXX_FLAGS_DEBUG_INIT "-g -Wall -Wextra")
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS}")

set(SOURCES
    src/app.cpp
    src/cameraController.cpp
    src/drawing.cpp
    src/enums.cpp
    src/game.cpp
    src/geometry.cpp
    src/hex.cpp
    src/hex_info.cpp
    src/house_menu.cpp
    src/inputProcessing.cpp
    src/main.cpp
    src/map.cpp
    src/menu.cpp
    src/newGameSetup.cpp
    src/options.cpp
    src/player.cpp
    src/shaderProcessing.cpp
    src/sounds.cpp
    src/trigger.cpp
)
add_executable(valley ${SOURCES})

target_link_libraries(valley PRIVATE ${CMAKE_SOURCE_DIR}/lib/libimgui.a)
target_link_libraries(valley PRIVATE ${CMAKE_SOURCE_DIR}/lib/libraylib.a)
target_link_libraries(valley PRIVATE ${CMAKE_DL_LIBS})
target_link_libraries(valley PRIVATE rt)
target_link_libraries(valley PRIVATE m) # math

set_target_properties(valley PROPERTIES CXX_CLANG_TIDY "${CLANG_TIDY_COMMAND}")
