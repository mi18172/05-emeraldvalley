# Define custom functions
rwildcard = $(wildcard $1$2) $(foreach d,$(wildcard $1*),$(call rwildcard,$d/,$2))

# Set global macros
buildDir := bin
executable := valley
target := $(buildDir)/$(executable)
sources := $(call rwildcard,src/,*.cpp)
objects := $(patsubst src/%, $(buildDir)/%, $(patsubst %.cpp, %.o, $(sources)))
depends := $(patsubst %.o, %.d, $(objects))
compileFlags := -std=c++17 -I include -I include/raylib -I include/imgui -fpic 
linkFlags = -L lib -l raylib -l imgui

linkFlags += -l GL -l m -l pthread -l dl -l rt -l X11

# Lists phony targets for Makefile
.PHONY: all fast exe clean

# Default target, compiles, executes and cleans
all: $(target) exe clean

fast: $(target)

# Link the program and create the executable
$(target): $(objects)
	g++ $(objects) -o $(target) $(linkFlags)

# Add all rules from dependency files
-include $(depends)

# Compile objects to the build directory

$(buildDir)/%.o: src/%.cpp Makefile
	mkdir -p $(@D)
	g++ -MMD -MP -c $(compileFlags) $< -o $@ $(CXXFLAGS)

exe:
	$(target) $(ARGS)

clean:
	rm -rf ./$(buildDir)
