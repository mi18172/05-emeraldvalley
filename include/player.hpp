#ifndef __PLAYER__HPP
#define __PLAYER__HPP

#include "hex.hpp"
#include <iostream>
#include <string>
#include <vector>
class Hex;
class Player {
  public:
    Player(const std::string& name, unsigned int id, int bodovi)
        : m_name(name), m_id(id), m_bodovi(bodovi)
    {
    }
    ~Player();
    Player(const Player& p) = delete;
    const Player& operator=(const Player& p) = delete;
    void          DodajHex(Hex* polje);
    void          DodajDostupniHex(Hex* polje);
    void          SelectHexFromPool();
    void          SelectHexOnMap();
    void          Place();

  private:
    friend std::ostream& operator<<(std::ostream& out, const Player& p);
    std::string          m_name;
    unsigned int         m_id;
    int                  m_bodovi;
    std::vector<Hex*>    m_posedovani;
    std::vector<Hex*>    m_dostupni;
};

#endif // __PLAYER__HPP
