#ifndef _ENUMS_
#define _ENUMS_

#include <string>

enum class hexID {
    CABIN,
    CASTLE,
    DOCK,
    FARM,
    HOUSE,
    MARKET,
    MILL,
    MINE,
    SHEEP,
    SMELTER,
    TOWER,
    VILLAGE,
    WATER,
    GRASS,
    FOREST,
    HILL,
    RIVER,
    RIVERV,
    RIVERL,
    RIVERSTART,
    MOUNTAIN,
    ROCKS,
    ISLAND,
    WATERROCKS,
    STONEHILL,
    LUMBER,
    WATERMILL,
    EMPTY,
    NONE
};

enum class cat {
    TOWN_BUILDING,
    FORTIFICATION,
    INDUSTRY,
    FOREST,
    LAND,
    ELEVATION,
    RIVER,
    SEA,
    WATERMILL,
    EMPTY
};

enum lightType { REG, SEL, INVALID, P1, P2, P3, P4 };

enum SoundID { SELECT, PLACE, ROTATE, ERROR, CLICK, PROMPT, VICTORY, DRAW, TIMER };

template <typename Enumeration>
auto as_integer(Enumeration const value) -> typename std::underlying_type<Enumeration>::type
{
    return static_cast<typename std::underlying_type<Enumeration>::type>(value);
}

extern std::string hexIDstring[];
extern std::string getToolTipFromID(hexID h);

#endif