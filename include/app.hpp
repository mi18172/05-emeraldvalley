#ifndef _APP_HPP_
#define _APP_HPP_

#include "cameraController.hpp"
#include "enums.hpp"
#include "hex.hpp"
#include "map.hpp"
#include "options.hpp"
#include "sounds.hpp"
#include <raylib-cpp.hpp>
#include <vector>

// namespace ImGui
// {
//     // ImGui::InputText() with std::string
//     // Because text input needs dynamic resizing, we need to setup a callback to grow the
//     capacity IMGUI_API bool  InputText(const char* label, std::string* str, ImGuiInputTextFlags
//     flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data = NULL); IMGUI_API bool
//     InputTextMultiline(const char* label, std::string* str, const ImVec2& size = ImVec2(0, 0),
//     ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data =
//     NULL); IMGUI_API bool  InputTextWithHint(const char* label, const char* hint, std::string*
//     str, ImGuiInputTextFlags flags = 0, ImGuiInputTextCallback callback = NULL, void* user_data =
//     NULL);
// }

class App {
  public:
    void run();

  private:
    void initializeApp();
    void init();
    void initImgui(int w, int h);
    void render();
    float inline getHexSpacing() { return hexSpacing; }
    void   DrawHexes(hexMap* map, bool b, int* currentPlayer, Camera* camera);
    void   DrawHex(Hex* h, bool b, int* currentPlayer, Camera* camera);
    Shader makeShader();
    void   applyShader(Hex& h);
    void   setLights(lightType t);
    Camera makeCamera();
    bool   mouseInPool();
    void   processInput(Map& hexMap, Camera& camera, cameraController* cCtrl, hexID h,
                        int* currentPlayer, soundPlayer* sounds, std::vector<int>* scores);
    void   setLastClickedId(hexID h);
    hexID inline getLastClickedId() { return lastClickedID; }
    void  setPoolClear(bool b);
    Color getColor(int* p);
    void  drawEscMenu(Options* o, soundPlayer* s);
    void  drawPlayerScores(std::vector<int>* scores);
    int   getWinner(std::vector<int>* scores);
    void  drawSelectedTooltip();
    void  drawHotkeys();

    int         mapRadius = 2;
    const float hexSpacing = 0.995;
    Shader      shader;
    HexBasic*   pickedHex = nullptr;
    int         screenWidth = 1600;
    int         screenHeight = 900;
    hexID       lastClickedID = hexID::NONE;
    // selection
    bool                selected;
    bool                placeable = false;
    std::pair<int, int> selectedHex;
    hexID               selectedHexID = hexID::NONE;
    // pool signaller to refresh
    bool  shouldClear = false;
    bool  showPlayerColors = true;
    bool  showHexScores = true;
    bool  shouldEndTurn = false;
    bool  GameOver = false;
    bool  gameShouldClose = false;
    bool  showOptions = false;
    bool  showHotkeys = false;
    Music m_music;
    bool  animateHexMovement = true;
};

#endif //_APP_HPP_
