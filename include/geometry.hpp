Vector3 getRayIntersectionPoint(Camera camera);

std::pair<int, int> spaceToHex(Vector3 position, float hexSpacing);