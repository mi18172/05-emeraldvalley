# Project EmeraldValley

Video igra u kojoj se na mrežu postavljaju izgenerisana polja koja sadrže različite objekte (prirodne i veštačke) koji se međusobno sklapaju

## Developers

- [Radovan Božić, 172/2018](https://gitlab.com/mi18172)
- [Petar Barić, 430/2019](https://gitlab.com/petar_baric)
- [Saška Keneški, 298/2016](https://gitlab.com/keneshki)
- [Zlatko Keneški, 75/2018](https://gitlab.com/Dyslexoid)

## Demo video

[Demo video](https://drive.google.com/file/d/1K_vyK-qdVqS96xEQzt-iekiTn7DAGRQQ/view?usp=sharing)

### Kompatibilnost
| OS          | Default Compiler |  Last Manual Build   | 
| ----------- | ---------------- | -------------------  | 
| **Linux**   | G++              | `Manjaro 21.1`       | 
| **Windows** | MinGW (G++)      | ???                  | 
| **macOS**   | Clang++          | ???                  | 


### Build
Biblioteke (raylib, Dear ImGui) su pre-built. Nakon što je kloniran ovaj repozitorijum, kopiran/otpakovan repozitorijum i instalirani dependencies, build-ovanje i kompilacija projekta se vrše uz pomoć _make_ komande u korenom direktorijumu:

```console
$ make
```

Ova komanda će takođe zatim pokrenuti projekat, a nakon završavanja programa obrisati generisane datoteke.

Ukoliko ne želite da čekate kompajler svaki put, može se izvršiti komanda:

```console
$ make fast
```

Nakon ove komande, moguće je pokrenuti program bez build-ovanja uz pomoć:

```console
$ make exe
```
