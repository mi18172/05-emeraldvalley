#include "../include/app.hpp"

auto main() -> int
{
    App* app = new App();

    app->run();

    delete app;
    return 0;
}
