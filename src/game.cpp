#include "../include/game.hpp"

#include <iostream>

Game::Game() = default;

void Game::endTurn()
{
    currentPlayer = (currentPlayer % numOfPlayers + 1);
    if (currentPlayer == 1)
        currentRound++;
}

void Game::setNextPlayer() { currentPlayer = (currentPlayer + 1) % numOfPlayers; }

auto Game::updateTimer() -> bool
{
    timerVal -= GetFrameTime();
    if (timerVal <= 0) {
        resetTimer();
        return true;
    }
    return false;
}
