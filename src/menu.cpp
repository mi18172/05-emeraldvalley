#include "../include/menu.hpp"

Setup   s;
Options o;

Menu::Menu() = default;

void Menu::drawMainMenu()
{

    ImGui::Begin("MainMenu", nullptr, window_flags);
    ImGui::SetNextWindowPos(ImVec2(GetScreenWidth() / 2 - width / 2, GetScreenHeight() / 2 - 300));
    ImGui::SetNextWindowSize(ImVec2(400, 350));
    ImGui::PushStyleColor(ImGuiCol_Button, (ImVec4)ImColor::HSV(0.138f, 0.5f, 0.5f, 0.65));
    ImGui::PushStyleColor(ImGuiCol_ButtonHovered, (ImVec4)ImColor::HSV(0.138f, 0.7f, 0.5f, 0.65));
    ImGui::PushStyleColor(ImGuiCol_ButtonActive, (ImVec4)ImColor::HSV(0.138f, 0.9f, 0.5f, 0.65));
    if (ImGui::Button("New Game", ImVec2(width, 50))) {
        s.setSetupFlag(true);
        setInMainMenuFlag(false);
    }
    ImGui::Spacing();
    if (ImGui::Button("Options", ImVec2(width, 50))) {
        setInMainMenuFlag(false);
        o.setOptionsFlag(true);
    }
    ImGui::Spacing();
    if (ImGui::Button("Quit Game", ImVec2(width, 50)))
        m_gameShouldClose = true;
    ImGui::PopStyleColor(3);

    ImGui::End();
}

void Menu::initMainMenu()
{
    ImGui::Begin("MainMenu", nullptr, window_flags);
    ImGui::SetNextWindowPos(ImVec2(GetScreenWidth() / 2 - width / 2, GetScreenHeight() / 2 - 300));
    ImGui::SetNextWindowSize(ImVec2(400, 350));
    if (getInMainMenuFlag())
        drawMainMenu();
    if (o.getOptionsFlag()) {
        o.drawOptions();
    }
    if (s.getSetupFlag()) {
        s.drawSetup();

        if (s.isGameReady) {
            timer = s.getSetupTimer();
            numOfPlayers = s.getNumPlayers();
        }
    }
    ImGui::End();
    ImGui::Render();
    ImGui_ImplOpenGL3_RenderDrawData(ImGui::GetDrawData());
}

auto Menu::IsGameReady() const -> bool { return s.isGameReady; }
