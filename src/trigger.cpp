#include "../include/trigger.hpp"
#include "../include/app.hpp"
#include <set>

std::vector<MyTexture> buttons;

Trigger::Trigger() = default;

std::vector<std::string> filePathsImg = {
    "resources/images/cbn.png",
    // "resources/images/cst.png",
    "resources/images/dok.png", "resources/images/frm.png", "resources/images/frs.png",
    "resources/images/grs.png", "resources/images/hil.png", "resources/images/hos.png",
    "resources/images/isl.png", "resources/images/lmb.png", "resources/images/mil.png",
    "resources/images/min.png", "resources/images/mrk.png", "resources/images/mtn.png",
    "resources/images/rck.png", "resources/images/rvl.png", "resources/images/rvr.png",
    "resources/images/rvs.png", "resources/images/rvv.png", "resources/images/shp.png",
    "resources/images/smt.png",
    // "resources/images/snd.png",
    "resources/images/sth.png", "resources/images/twr.png", "resources/images/vlg.png",
    "resources/images/wtm.png", "resources/images/wtr.png"};

void Trigger::drawPool(bool* shouldClear)
{

    // ImGui::SetNextWindowPos(ImVec2((GetScreenWidth()*0.35), GetScreenHeight()*0.75));
    // ImGui::SetNextWindowSize(ImVec2((GetScreenWidth()*0.3), GetScreenHeight()*0.15));
    ImGui::SetNextWindowPos(ImVec2(525, 760));
    ImGui::SetNextWindowSize(ImVec2(550, 110));
    ImGui::SetNextWindowBgAlpha(0.5f);
    ImGui::Begin("pool", nullptr, window_flags);
    static int buttonNo = 5;
    srand(time(nullptr));
    for (int i = 0; i < buttonNo; i++) {
        ImGui::PushID(i);
        int    frame_padding = 10;          // -1 == uses default padding (style.FramePadding)
        ImVec2 size = ImVec2(80.0f, 80.0f); // Size of the image we want to make visible
        ImVec2 uv0 = ImVec2(0.0f, 0.0f);    // UV coordinates for lower-left
        ImVec2 uv1 = ImVec2(1.0f, 1.0f);    // UV coordinates for (32,32) in our texture
        ImVec4 bg_col = ImVec4(0.0f, 0.0f, 0.0f, 0.0f);   // Black background
        ImVec4 tint_col = ImVec4(1.0f, 1.0f, 1.5f, 1.0f); // No tint

        std::set<int> fp;
        while (fp.size() < 5) {
            fp.insert(rand() % filePathsImg.size());
        }

        if (buttons.size() != buttonNo) {
            for (auto s : fp) {
                Texture2D poolPic = LoadTexture(filePathsImg[s].c_str());
                buttons.push_back({
                    texture : poolPic,
                    path : filePathsImg[s],
                });
            }
        }
        if (ImGui::ImageButton((ImTextureID)buttons[i].texture.id, size, uv0, uv1, frame_padding,
                               bg_col, tint_col)) {
            setPathFlag(true);
            updateButton(i);
            updatePath(buttons[i].path.c_str());
        }

        if (ImGui::IsItemHovered()) {
            ImGui::BeginTooltip();
            ImGui::Text(getToolTipFromID(getIDfromImagePath(buttons[i].path)).c_str());
            ImGui::EndTooltip();
        }

        if (*shouldClear) {
            buttons.clear();
            *shouldClear = false;
        }
        ImGui::PopID();
        ImGui::SameLine();
    }

    ImGui::End();
}

auto Trigger::getButton() -> int { return m_button; }

void Trigger::updateButton(unsigned int button) { m_button = button; }
auto Trigger::getPath() -> hexID
{
    setPathFlag(false);
    staticPath = m_path;
    return getIDfromImagePath(m_path);
}

void Trigger::updatePath(std::string path) { m_path = path; }