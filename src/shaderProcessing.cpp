#include "app.hpp"
#define RLIGHTS_IMPLEMENTATION
#include "../include/raylib/rlights.hpp"

Light lights[MAX_LIGHTS] = {0};

// TODO optimize light switching by rendering hexes player by player

auto App::makeShader() -> Shader
{
    shader = LoadShader(("resources/shaders/base_lighting.vs"), ("resources/shaders/lighting.fs"));
    shader.locs[SHADER_LOC_VECTOR_VIEW] = GetShaderLocation(shader, "viewPos");
    // Shader setup
    int   ambientLoc = GetShaderLocation(shader, "ambient");
    float temp[4] = {0.1f, 0.1f, 0.1f, 1.0f};
    SetShaderValue(shader, ambientLoc, temp, SHADER_UNIFORM_VEC4);
    lights[0] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){50, 40, 20}, Vector3Zero(),
                            {180, 180, 150, 255}, shader); // GENERAL
    lights[1] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {255, 255, 255, 255}, shader); // SELECTED HEX
    lights[2] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {255, 100, 100, 255}, shader); // IMPROPER PLACEMENT
    lights[3] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {50, 50, 250, 255}, shader); // BLUE PLAYER 1
    lights[4] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {255, 100, 0, 255}, shader); // ORANGE PLAYER 2
    lights[5] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {50, 250, 50, 255}, shader); // GREEN PLAYER 3
    lights[6] = CreateLight(LIGHT_DIRECTIONAL, (Vector3){0, 40, 0}, Vector3Zero(),
                            {191, 85, 200, 255}, shader); // PURPLE PLAYER 4

    lights[1].enabled = false;
    lights[2].enabled = false;
    lights[3].enabled = false;
    lights[4].enabled = false;
    lights[5].enabled = false;
    lights[6].enabled = false;
    UpdateLightValues(shader, lights[0]);
    UpdateLightValues(shader, lights[1]);
    UpdateLightValues(shader, lights[2]);
    UpdateLightValues(shader, lights[3]);
    UpdateLightValues(shader, lights[4]);
    UpdateLightValues(shader, lights[5]);
    UpdateLightValues(shader, lights[6]);
    return shader;
}

void App::applyShader(Hex& h)
{
    for (int j = 0; j < (h.getModel())->materialCount; j++)
        (h.getModel())->materials[j].shader = shader;
}

void App::setLights(lightType t)
{
    for (int i = 0; i < 7; i++) {
        if (i == t)
            lights[i].enabled = true;
        else
            lights[i].enabled = false;
    }
    for (auto& light : lights) {
        UpdateLightValues(shader, light);
    }
}
